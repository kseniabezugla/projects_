
const selectQuestions = undefined;
const selectQuestion = undefined;
const selectContent = undefined;
var selectCorrect;
const buttonClickStart = document.querySelector('.section_button_start');
const buttonClickSkip = document.querySelector('.section_button_skip');
const currentQuestion = document.querySelector('.question');
const firstResponse = document.querySelector('.response1');
const secondResponse = document.querySelector('.response2');
const thirdResponse = document.querySelector('.response3');
const fourthResponse = document.querySelector('.response4');
const responseDiv = document.querySelector('.responses');
let priceChanged = document.querySelector('.price');
let prizeChanged = document.querySelector('.prize');
let priceNumber = 0;
let prizeNumber = 100;
let isSkiped = false;


const mainQuestions = () => {
    const selectQuestions = window.questions[Math.ceil(Math.random(0) * 74)];
    const selectQuestion = selectQuestions['question'];
    const selectContent = selectQuestions['content'];
    selectCorrect = selectQuestions;
    currentQuestion.textContent = selectQuestion;
    firstResponse.textContent = selectContent[0];
    secondResponse.textContent = selectContent[1];
    thirdResponse.textContent = selectContent[2];
    fourthResponse.textContent = selectContent[3];
}
buttonClickStart.onclick = () => {
    mainQuestions();
    priceChanged.textContent = 0;
    prizeChanged.textContent = 100;
}

buttonClickSkip.onclick = () => {
    if (!isSkiped) {
        mainQuestions()
        isSkiped = true;
    }
}

responseDiv.onclick = (event) => {
    if (event.target.dataset.responses !== 'response-all') {
        return
    }
    if (this.selectCorrect.content[this.selectCorrect.correct] === event.target.innerHTML) {
        mainQuestions();
        priceChanged.innerHTML = '';
        priceNumber += prizeNumber;
        prizeNumber *= 2;
        priceChanged.innerHTML = priceNumber;
        prizeChanged.innerHTML = '';
        prizeChanged.innerHTML = prizeNumber;
        return priceChanged;
    } else {
        isSkiped = false;
        alert(' G A M E  O V E R !!!');
        mainQuestions();
        priceChanged.innerHTML = 0;
        prizeChanged.innerHTML = 100;
    }
}