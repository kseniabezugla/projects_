import axios from 'axios';

const http = axios.create({
  baseURL: 'https://restcountries.eu',
});

export const getCountries = name => {
  let result = http
    .get(`/rest/v2/name/${name}`)
    .then(response => {
      console.log(response.data);
      return response.data;
    })
    .catch(error => {
      console.log(error.response);
      throw new Error(error);
    });
  return result;
};
