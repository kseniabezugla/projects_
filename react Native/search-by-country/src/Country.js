import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Country = ({choice}) => {
  const {h2, textCards, cards} = styles;
  return (
    <View>
      <Text style={h2}>{choice.name}</Text>
      <View style={textCards}>
        <Text style={cards}>Capital: {choice.capital}</Text>
        <Text style={cards}>Region: {choice.region}</Text>
        <Text style={cards}>Population: {choice.population}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  h2: {
    fontSize: 20,
    textAlign: 'center',
    textTransform: 'uppercase',
    marginTop: 40,
    margin: 20,
    height: 40,
  },
  cards: {
    fontFamily: 'sans-serif',
    fontSize: 16,
  },
  textCards: {
    marginRight: 30,
    marginLeft: 30,
    borderColor: 'gainsboro',
    borderWidth: 1,
    padding: 10,
  },
});

export default Country;
