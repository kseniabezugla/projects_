import React, {useEffect, useState} from 'react';
import {getCountries} from './utils/requests';
import {useDebounce} from 'use-debounce';
import {View, Text, ScrollView, TextInput, StyleSheet} from 'react-native';
import Country from './Country';

const App = () => {
  const {searchStyle, inputStyle, countryStyle} = styles;
  const [searchValue, setSearchValue] = useState('');
  const [result, setResult] = useState([]);
  const [isSearching, setIsSearching] = useState(false);
  const [country, setCountry] = useState(false);
  const [choice, setChoice] = useState([]);
  const [debounced] = useDebounce(searchValue, 1000);

  useEffect(() => {
    if (debounced) {
      setIsSearching(true);
      getCountries(debounced).then(results => {
        setIsSearching(false);
        setResult(results);
      });
    } else {
      setResult([]);
    }
  }, [debounced]);

  return country ? (
    <Country choice={choice} />
  ) : (
    <View>
      <TextInput
        style={inputStyle}
        placeholder="Enter country"
        onChangeText={text => setSearchValue(text)}
      />

      {isSearching ? (
        <Text style={searchStyle}>Searching...</Text>
      ) : (
        <ScrollView>
          {result.map((res, index) => (
            <View key={index} style={countryStyle}>
              <Text
                onPress={() => {
                  setIsSearching(false);
                  setChoice(res);
                  setCountry(true);
                }}>
                {res.name}
              </Text>
            </View>
          ))}
        </ScrollView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    marginTop: 40,
    margin: 20,
    borderRadius: 4,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
  },
  searchStyle: {
    textAlign: 'center',
  },
  countryStyle: {
    marginLeft: 20,
    borderColor: 'gainsboro',
    borderWidth: 1,
    margin: 4,
    marginRight: 20,
    padding: 5,
  },
});

export default App;
