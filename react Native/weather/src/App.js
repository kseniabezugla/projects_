import React, {useState} from 'react';
import {useEffect} from 'react';
import {ScrollView, Text, View, ImageBackground, Image} from 'react-native';
import {getWeather} from './utils/requests';
import Geocoder from 'react-native-geocoding';
import {images} from './utils/images';
import {dataFormat} from './utils/functions';
import {getCurrentDate} from './utils/functions';
import {newWeather} from './utils/functions';
import {styles} from './utils/styles';
import GetLocation from 'react-native-get-location';

const App = () => {
  const [location, setLocation] = useState();
  const [address, setAddress] = useState('');
  const [weather, setWeather] = useState([]);
  const [loading, setLoading] = useState(false);
  const {
    h1,
    tempStyle,
    imagesStyle,
    borders,
    imageStyle,
    textStyle,
    scrollStyle,
    loadForm,
    dataStyle,
    wrapperData,
  } = styles;
  const image = {
    uri:
      'https://img4.goodfon.ru/original/1920x1200/3/c3/oblaka-nebo-sky-clouds.jpg',
  };

  useEffect(() => {
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(location => {
        setLocation([location.latitude, location.longitude]);
      })
      .catch(error => {
        const {code, message} = error;
        console.warn(code, message);
      });
  }, []);

  useEffect(() => {
    setLoading(true);
    if (location) {
      getWeather(location).then(data => setWeather(data.dataseries));
      getAddress();
    }
  }, [location]);

  const getAddress = async () => {
    try {
      Geocoder.init('AIzaSyDsMbKe6O4KUoUikUcEt56ZALqgQwtUfM0');
      const response = await Geocoder.from(location);
      let addressComponent = response.results[0].formatted_address;
      setAddress(addressComponent);
    } catch (error) {
      console.warn(error);
    }
  };

  return (
    <View>
      <ImageBackground source={image} style={imageStyle}>
        <Text style={h1}>Weather</Text>
        <Text style={textStyle}>{address}</Text>
        {loading && newWeather(weather).length === 0 ? (
          <Text style={loadForm}>Loading...</Text>
        ) : (
          <ScrollView style={scrollStyle}>
            {newWeather(weather).map((item, index) => (
              <View key={index}>
                <Text style={dataStyle}>{getCurrentDate(index)}</Text>
                <View style={borders}>
                  {item.map(item => (
                    <View style={wrapperData} key={item.timepoint}>
                      <Text>{dataFormat(item.timepoint)}</Text>
                      <Image
                        style={imagesStyle}
                        source={images(item.weather)}
                      />
                      <Text style={tempStyle}>{item.temp2m}°</Text>
                    </View>
                  ))}
                </View>
              </View>
            ))}
          </ScrollView>
        )}
      </ImageBackground>
    </View>
  );
};

export default App;
