export const images = nameImage => {
  switch (nameImage) {
    case 'clearday':
      return require('../images/clearday.png');
    case 'clearnight':
      return require('../images/clearnight.png');
    case 'cloudyday':
      return require('../images/cloudyday.png');
    case 'cloudynight':
      return require('../images/cloudynight.png');
    case 'humidday':
      return require('../images/humidday.png');
    case 'humidnight':
      return require('../images/humidnight.png');
    case 'lightrainday':
      return require('../images/lightrainday.png');
    case 'lightrainnight':
      return require('../images/lightrainnight.png');
    case 'oshowerday':
      return require('../images/oshowerday.png');
    case 'oshowernight':
      return require('../images/oshowernight.png');
    case 'mcloudyday':
      return require('../images/mcloudyday.png');
    case 'mcloudynight':
      return require('../images/mcloudynight.png');
    case 'pcloudyday':
      return require('../images/pcloudyday.png');
    case 'pcloudynight':
      return require('../images/pcloudynight.png');
    case 'ishowerday':
      return require('../images/ishowerday.png');
    case 'ishowernight':
      return require('../images/ishowernight.png');
    case 'rainday':
      return require('../images/rainday.png');
    case 'rainnight':
      return require('../images/rainnight.png');
    case 'tsrainday':
      return require('../images/tsrainday.png');
    case 'tsrainnight':
      return require('../images/tsrainnight.png');
    default:
      console.log(nameImage);
      break;
  }
};
