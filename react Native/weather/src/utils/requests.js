import axios from 'axios';

export const getWeather = async ([lon, lat]) => {
  try {
    const response = await axios.get(
      `http://www.7timer.info/bin/civil.php?lon=${lon}&lat=${lat}&ac=0&unit=metric&output=json&tzshift=0`,
    );
    return response.data;
  } catch (error) {
    console.log(error.response);
    throw new Error(error);
  }
};
