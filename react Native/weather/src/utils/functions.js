export const dataFormat = data => {
  const time = data % 24;
  return `${time.toString().padStart(2, '0')}:00`;
};

export const getCurrentDate = day => {
  var date = new Date().getDate();
  var month = new Date().getMonth() + 1;
  var year = new Date().getFullYear();
  return `${(date + day)
    .toString()
    .padStart(2, '0')} - ${month.toString().padStart(2, '0')} - ${year}`;
};

export const newWeather = weather => {
  return weather
    .map((item, i) => {
      return weather.slice(0 + 8 * i, 8 * (i + 1));
    })
    .filter(item => item.length);
};
