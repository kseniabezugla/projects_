import React, {useEffect, useState} from 'react';
import {getComics} from './utils/requests';
import {View, Text, Image, Dimensions} from 'react-native';
import {styles} from './utils/styles';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

const App = () => {
  const {h1, img, imgLandscape} = styles;
  const [comics, setComics] = useState([]);
  const [pages, setPages] = useState(Math.floor(Math.random() * 2300));
  const [orientation, setOrientation] = useState();

  const isPortrait = () => {
    const dim = Dimensions.get('window');
    return dim.width < dim.height;
  };

  useEffect(() => {
    getComics(pages).then(response => {
      setComics(response);
    });
    setOrientation(isPortrait() ? 'portrait' : 'landscape');
  }, [pages, orientation]);

  console.log(pages);
  console.log(orientation);

  const onSwipe = gestureName => {
    const {SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
    switch (gestureName) {
      case SWIPE_LEFT:
        pages > 1 && setPages(pages + 1);
        break;
      case SWIPE_RIGHT:
        comics && setPages(pages - 1);
        break;
    }
  };
  const config = {
    velocityThreshold: 0.3,
    directionalOffsetThreshold: 130,
  };
  return (
    <GestureRecognizer
      onSwipe={direction => onSwipe(direction)}
      config={config}
      style={{
        flex: 1,
      }}>
      <View>
        <Text style={h1}>{comics.title}</Text>
        <Image
          style={[orientation === 'portrait' ? img : imgLandscape]}
          source={{uri: comics.img}}
        />
      </View>
    </GestureRecognizer>
  );
};

export default App;
