import axios from 'axios';

export const getComics = async page => {
  try {
    const response = await axios.get(`https://xkcd.com/${page}/info.0.json`);
    return response.data;
  } catch (error) {
    console.log(error.response);
    throw new Error(error);
  }
};
