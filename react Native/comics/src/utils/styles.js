import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
  },
  h1: {
    fontSize: 20,
    textTransform: 'uppercase',
    textAlign: 'center',
    marginTop: 40,
    margin: 20,
    height: 40,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
  img: {
    width: 'auto',
    resizeMode: 'contain',
    overlayColor: 'white',
    margin: 10,
    minHeight: 350,
    borderWidth: 1,
  },
  imgLandscape: {
    width: 'auto',
    resizeMode: 'contain',
    overlayColor: 'white',
    minHeight: 260,
    borderWidth: 1,
  },
});
