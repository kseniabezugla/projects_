import axios from 'axios';

export const getCrypto = async (amount, pages) => {
  try {
    const response = await axios.get(
      `https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=${amount}&page=${pages}&sparkline=false`,
    );
    return response.data;
  } catch (error) {
    console.log(error.response);
    throw new Error(error);
  }
};
