import React, {useEffect, useRef, useState} from 'react';
import {getCrypto} from './utils/requests';
import {View, Text, StyleSheet, FlatList, Pressable} from 'react-native';
import SelectPicker from 'react-native-form-select-picker';
import {styles} from './utils/styles';

const App = () => {
  const {
    h1,
    textStyle,
    titleStyle,
    fontStyle,
    selector,
    button,
    wrapper,
    containerCurrency,
    containerButtons,
    textButtonStyle,
    currentPage,
  } = styles;

  const [crypto, setCrypto] = useState([]);
  const [pages, setPages] = useState(1);
  const [selected, setSelected] = useState('20');
  const [showButtons, setShowButtons] = useState(false);
  const options = ['20', '40', '60', '80'];
  const flatlistRef = useRef();

  goToTop = () => {
    flatlistRef.current.scrollToOffset({animated: true, offset: 0});
  };

  useEffect(() => {
    getCrypto(selected, pages).then(response => {
      setCrypto(response), setShowButtons(false), goToTop();
    });
  }, [selected, pages]);

  return (
    <View style={containerCurrency}>
      <Text style={h1}>Cryptocurrency Prices</Text>
      <View style={wrapper}>
        <View style={selector}>
          <SelectPicker
            placeholder="Number of values ​​per page   "
            onValueChange={value => {
              setSelected(value);
              if (value) {
                setPages(1);
              }
            }}
            selected={selected}>
            {Object.values(options).map((val, index) => (
              <SelectPicker.Item
                key={index}
                 label={`Number of values ​​per page: ${val}`}
                value={val}
              />
            ))}
          </SelectPicker>
        </View>
        <View>
          <Text style={currentPage}>Current page: {pages}</Text>
        </View>
      </View>
      <View style={titleStyle}>
        <Text style={fontStyle}>Coin</Text>
        <Text style={fontStyle}>Price</Text>
      </View>

      {Boolean(crypto.length) && (
        <FlatList
          ref={flatlistRef}
          onEndReached={distanceFromEnd => {
            distanceFromEnd && setShowButtons(true);
          }}
          onEndReachedThreshold={0.01}
          data={crypto}
          keyExtractor={index => index.id}
          renderItem={({item}) => (
            <View style={textStyle}>
              <Text>{item.name}</Text>
              <Text> {item.current_price}</Text>
            </View>
          )}
        />
      )}

      {showButtons && (
        <View style={containerButtons}>
          <Pressable
            style={button}
            onPress={() => {
              pages > 1 && setPages(pages - 1);
            }}>
            <Text style={textButtonStyle}>{'<'}</Text>
          </Pressable>
          <Pressable
            style={button}
            onPress={() => {
              setPages(pages + 1);
            }}>
            <Text style={textButtonStyle}>{'>'}</Text>
          </Pressable>
        </View>
      )}
    </View>
  );
};

export default App;
