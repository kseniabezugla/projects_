import React, {useEffect, useState} from 'react';
import {styles} from './utils/styles';
import {SwipeListView} from 'react-native-swipe-list-view';
import {Text, Pressable, View, TouchableOpacity} from 'react-native';
import ModalWindow from './Modal';
import {getData} from './utils/storeData';
import {storeData} from './utils/storeData';

const App = () => {
  const {
    h1,
    container,
    button,
    textStyle,
    backTextWhite,
    rowFront,
    rowBack,
    backRightBtn,
    backRightBtnRight,
    textIndicator,
    textList,
    blurStyle,
    scrollStyle,
    whiteBlock,
  } = styles;

  const [modal, setModal] = useState(false);
  const [todoList, setTodoList] = useState([]);
  const [error, setError] = useState(false);
  const [todo, setTodo] = useState('');
  const [index, setIndex] = useState(null);

  useEffect(() => {
    getData().then(response => setTodoList(response));
  }, []);

  const deleteRow = index => {
    const newData = [...todoList];
    newData.splice(index, 1);
    setTodoList(newData);
    storeData(newData);
  };

  const modifyRow = index => {
    const currentData = todoList[index];
    setTodo(currentData);
    setError(false);
    setModal(true);
    setIndex(index + 1);
  };

  const renderHiddenItem = data => (
    <View style={rowBack}>
      <TouchableOpacity
        onPress={() => {
          modifyRow(data.index);
        }}>
        <Text>Modify</Text>
      </TouchableOpacity>
      <View style={whiteBlock}></View>
      <TouchableOpacity
        style={[backRightBtn, backRightBtnRight]}
        onPress={() => {
          deleteRow(data.index);
        }}>
        <Text style={backTextWhite}>Delete</Text>
      </TouchableOpacity>
    </View>
  );

  const renderItem = data => (
    <View style={rowFront} key={data.item.key}>
      <Text style={textIndicator}>- </Text>
      <Text style={textList}>{data.item.text}</Text>
    </View>
  );

  return (
    <View style={[container, modal ? blurStyle : null]}>
      <Text style={h1}>TODO list</Text>
      {modal && (
        <ModalWindow
          error={error}
          todo={todo}
          modal={modal}
          todoList={todoList}
          index={index}
          setError={setError}
          setTodo={setTodo}
          setModal={setModal}
          setTodoList={setTodoList}
        />
      )}
      <Pressable
        style={button}
        onPress={() => {
          setIndex(null);
          setModal(true);
          setError(false);
          setTodo('');
        }}>
        <Text style={textStyle}>Add</Text>
      </Pressable>
      <View style={scrollStyle}>
        <SwipeListView
          data={todoList.map((element, key) => {
            return {key: key + Math.floor(Math.random() * 1000), text: element};
          })}
          renderItem={renderItem}
          renderHiddenItem={renderHiddenItem}
          leftOpenValue={75}
          rightOpenValue={-75}
          previewRowKey={'0'}
          previewOpenValue={0}
          previewOpenDelay={2000}
        />
      </View>
    </View>
  );
};

export default App;
