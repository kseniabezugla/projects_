import React from 'react';
import {Modal, Text, Pressable, View, TextInput} from 'react-native';
import {styles} from './utils/styles';
import {storeData} from './utils/storeData';

const ModalWindow = ({
  modal,
  error,
  todo,
  setTodo,
  setModal,
  todoList,
  setTodoList,
  setError,
  index,
}) => {
  const {
    container,
    modalView,
    button,
    textStyle,
    inputStyle,
    errorStyle,
    closeStyle,
  } = styles;

  const handlerCreate = () => {
    if (todo.length > 0) {
      setModal(false);
      setTodoList([...todoList, todo]);
      storeData([...todoList, todo]);
    } else {
      setError(true);
    }
  };

  const handlerModify = () => {
    todoList[index - 1] = todo;
    if (todo.length > 0) {
      setModal(false);
      setTodoList([...todoList]);

      storeData(todoList);
    } else {
      setError(true);
    }
  };
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modal}
      onRequestClose={() => {
        setModal(false);
      }}>
      <View style={container}>
        <View style={modalView}>
          <Text style={closeStyle} onPress={() => setModal(false)}>
            x
          </Text>
          {error && <Text style={errorStyle}>Enter todo!</Text>}
          <TextInput
            style={inputStyle}
            placeholder="Enter TODO "
            defaultValue={todo}
            onChangeText={text => setTodo(text)}
            
          />
          <Pressable
            style={button}
            onPress={index ? handlerModify : handlerCreate}
           >
            <Text style={textStyle}>Submit</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};
export default ModalWindow;
