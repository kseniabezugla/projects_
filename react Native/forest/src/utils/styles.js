import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  img: {
    width: 400,
    resizeMode: 'cover',
    height: 650,
  },
});
