import {Image, Text, View} from 'react-native';
import React from 'react';
import {styles} from './utils/styles';

const SecondScreen = () => {
  const {img} = styles;
  return (
    <View>
      <Image source={require('./assets/tree.jpg')} style={img} />
    </View>
  );
};
export default SecondScreen;
