import {Image, Text, View} from 'react-native';
import React from 'react';
import {styles} from './utils/styles';

const FirstScreen = () => {
  const {img} = styles;
  return (
    <View>
      <Image source={require('./assets/tepl.jpg')} style={img} />
    </View>
  );
};
export default FirstScreen;
