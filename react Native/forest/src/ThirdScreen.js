import {Image, Text, View} from 'react-native';
import React from 'react';
import {styles} from './utils/styles';

const ThirdScreen = () => {
  const {img} = styles;
  return (
    <View>
      <Image source={require('./assets/palms.jpg')} style={img} />
    </View>
  );
};
export default ThirdScreen;
