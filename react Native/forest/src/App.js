import React, {useEffect} from 'react';
import RNBootSplash from 'react-native-bootsplash';
import {NavigationContainer} from '@react-navigation/native';
import Tabs from './utils/tabs';

const App = () => {
  useEffect(() => {
    RNBootSplash.hide({duration: 350});
  }, []);

  return (
    <NavigationContainer>
      <Tabs />
    </NavigationContainer>
  );
};
export default App;
