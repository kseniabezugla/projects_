import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Cell from './Cell';
import {styles} from './utils/styles';
import {doBoard} from './utils/functions';
import {keyCases} from './utils/functions';

const boardSize = 3;
const itemsWin = 3;

const App = () => {
  const {
    containerStyle,
    row,
    table,
    title,
    winStyle,
    buttonStyle,
    textButton,
  } = styles;
  const board = doBoard(Array(boardSize * boardSize).fill(''), boardSize);
  const [next, setNext] = useState(true);
  const [resultBoard, setResultBoard] = useState(board);
  const [winner, setWinner] = useState(false);
  const [draw, setDraw] = useState(false);
  const currentStep = next ? 'X' : 'O';

  const arrayData = resultBoard
    .map((idRow, indexRow) =>
      idRow.map((itemCol, indexCol) => {
        return {
          item: resultBoard[indexRow][indexCol],
          indexRow,
          indexCol,
        };
      }),
    )
    .flat()
    .filter(e => e.item);

  const listCases = [
    keyCases('line', arrayData, 'R_'),
    keyCases('line', arrayData, 'C_'),
    keyCases('diagonal', arrayData, 'DiaP_'),
    keyCases('diagonal', arrayData, 'DiaS_'),
  ];

  useEffect(() => {
    listCases.forEach(item =>
      Object.values(item).forEach(item => {
        item === itemsWin && setWinner(true);
      }),
    );
  });

  useEffect(() => {
    if (!winner) return;
    setNext(!next);
  }, [winner]);

  useEffect(() => {
    if (arrayData.length === boardSize * boardSize) {
      setDraw(true);
    }
  });

  const handleClear = () => {
    setWinner(false);
    setDraw(false);
    setResultBoard(board);
  };

  return (
    <View style={containerStyle}>
      <Text style={title}>Tic Tac Toe</Text>
      {draw && <Text style={winStyle}>DRAW!!!</Text>}
      {winner && <Text style={winStyle}>Winner: {currentStep}</Text>}
      <View style={table}>
        {resultBoard.map((item, indexRow) => (
          <View style={row} key={indexRow}>
            {item.map((col, indexCol) => (
              <Cell
                key={indexCol}
                indexRow={indexRow}
                indexCol={indexCol}
                next={next}
                resultBoard={resultBoard}
                setNext={setNext}
                setResultBoard={setResultBoard}
                winner={winner}
                currentStep={currentStep}
              />
            ))}
          </View>
        ))}
      </View>
      {(winner || draw) && (
        <TouchableOpacity style={buttonStyle} onPress={handleClear}>
          <Text style={textButton}>PLAY AGAIN</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};
export default App;
