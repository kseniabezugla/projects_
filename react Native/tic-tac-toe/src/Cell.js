import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {styles} from './utils/styles';

const Cell = ({
  indexCol,
  indexRow,
  next,
  setNext,
  resultBoard,
  setResultBoard,
  winner,
  currentStep,
}) => {
  const {container, content} = styles;

  const handlePress = () => {
    resultBoard[indexRow][indexCol] = currentStep;
    setResultBoard([...resultBoard]);
    setNext(!next);
  };

  return (
    <TouchableOpacity
      onPress={handlePress}
      disabled={resultBoard[indexRow][indexCol] || winner}
      currentStep={currentStep}>
      <View style={container}>
        <Text style={content}>{resultBoard[indexRow][indexCol]}</Text>
      </View>
    </TouchableOpacity>
  );
};
export default Cell;
