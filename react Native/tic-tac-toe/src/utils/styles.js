import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: 100,
    height: 100,
    margin: -2,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#1D39AB',
    borderWidth: 4,
  },
  title: {
    fontSize: 35,
    marginTop: 40,
    marginBottom: 120,
  },
  containerStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  table: {
    display: 'flex',
  },
  winStyle: {
    backgroundColor: '#8BB5EB',
    padding: 10,
    width: 400,
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: -97,
    marginBottom: 50,
    textAlign: 'center',
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#8BB5EB',
    padding: 15,
    marginTop: 20,
    borderRadius: 5,
  },
  textButton: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  content: {
    fontSize: 40,
  },
});
