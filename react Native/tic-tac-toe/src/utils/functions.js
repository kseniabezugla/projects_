export const doBoard = (arr, len) => {
  const newBoard = [];
  for (let i = 0; i < arr.length; ) {
    newBoard.push(arr.slice(i, (i += len)));
  }
  return newBoard;
};

export const keyCases = (typeLine, array, keyCase) => {
  const arrayCases = array.reduce((acc, n) => {
    if (typeLine === 'line') {
      const keyLine =
        keyCase + n.item + '_' + (keyCase === 'C_' ? n.indexCol : n.indexRow);
      acc[keyLine] = (acc[keyLine] || 0) + 1;
    }
    if (typeLine === 'diagonal') {
      const key = keyCase + n.item;
      (keyCase === 'DiaP_'
        ? n.indexCol === n.indexRow
        : n.indexCol + n.indexRow === 2) && (acc[key] = (acc[key] || 0) + 1);
    }
    return acc;
  }, {});
  return arrayCases;
};
