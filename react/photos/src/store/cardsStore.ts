import { makeObservable, observable } from "mobx";
import { instance } from "../utils/config";
import { Cards } from "../utils/interfaces";

class CardsStore {
  cards: Cards[] = [];
  rootStore: any;
  currentId!: number;
  loader: boolean = false;

  constructor(rootStore: any) {
    makeObservable(this, {
      cards: observable,
      currentId: observable,
      loader: observable,
    });
    this.rootStore = rootStore;
  }
  setCards = (newCards: Cards[]) => {
    this.cards = newCards;
  };
  setCurrentId = (newCurrentId: number) => {
    this.currentId = newCurrentId;
  };
  setLoader = (newLoader: boolean) => {
    this.loader = newLoader;
  };

  getCards = async () => {
    try {
      const response = await instance.get("/");
      this.setCards(response.data);
    } catch (error) {
      console.log(error);
    }
  };
}
export default CardsStore;
