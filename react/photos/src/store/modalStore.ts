import { AddComment, InfoImage } from "../utils/interfaces";
import { computed, makeObservable, observable } from "mobx";
import { instance } from "../utils/config";

class ModalStore {
  rootStore: any;
  modal: boolean = false;
  imageData!: InfoImage;
  comments: AddComment[] = [];

  constructor(rootStore: any) {
    makeObservable(this, {
      modal: observable,
      imageData: observable,
      comments: observable,
      existComment: computed,
    });
    this.rootStore = rootStore;
  }

  get existComment() {
    return this.comments.filter(
      (item) => item.id === this.rootStore.cardsStore.currentId
    );
  }

  setModal = (newModal: boolean) => {
    this.modal = newModal;
  };
  setImageData = (newImageData: InfoImage) => {
    this.imageData = newImageData;
  };

  setComments = (newComment: AddComment[]) => {
    this.comments = newComment;
  };

  getInfoImage = async () => {
    try {
      const response = await instance.get(
        `${this.rootStore.cardsStore.currentId}`
      );
      this.setImageData(response.data);
      this.rootStore.cardsStore.setLoader(false);
    } catch (error) {
      console.log(error);
    }
  };
}
export default ModalStore;
