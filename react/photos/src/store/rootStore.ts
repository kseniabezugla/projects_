import { createContext, useContext } from "react";
import CardsStore from "./cardsStore";
import ModalStore from "./modalStore";

class RootStore {
  constructor() {
    this.cardsStore = new CardsStore(this);
    this.modalStore = new ModalStore(this);
  }
  cardsStore: CardsStore;
  modalStore: ModalStore;
}

export const rootStore = new RootStore();

export const RootStoreContext = createContext<RootStore>({} as RootStore);

export const useRootStoreContext = () => useContext(RootStoreContext);
