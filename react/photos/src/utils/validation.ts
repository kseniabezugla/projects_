import * as Yup from "yup";

export const validation = {
  name: Yup.string()
    .min(1, "Too short!")
    .max(50, "Too long!")
    .required("Please Enter your name"),
  text: Yup.string()
    .min(2, "Too short!")
    .max(250, "Too long!")
    .required("Please leave a comment"),
};
