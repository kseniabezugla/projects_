import axios from "axios";

export const instance = axios.create({
  baseURL: "https://boiling-refuge-66454.herokuapp.com/images/",
});
