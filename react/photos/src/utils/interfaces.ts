export interface Cards {
  id: number;
  url: string;
}
export interface InfoImage {
  id: number;
  url: string;
  comments: Comment[];
}
export interface Comment {
  id: number;
  text: string;
  date: number;
}
export interface AddComment {
  name: string;
  text: string;
  id: number;
}
