import { observer } from "mobx-react";
import React, { useEffect } from "react";
import { useRootStoreContext } from "../../store/rootStore";
import CardsItem from "../cards-item/cards-item";
import ModalWindow from "../modal-window";
import "./App.css";

const App = observer(() => {
  const context = useRootStoreContext();
  const cardsStore = context.cardsStore;
  const modalStore = context.modalStore;

  useEffect(() => {
    cardsStore.getCards();
  }, [cardsStore]);

  return (
    <div className="App">
      {/* <header >
        <h1> Photos</h1>
      </header> */}
      <CardsItem />
      {modalStore.modal && <ModalWindow />}
    </div>
  );
});

export default App;
