import { observer } from "mobx-react";
import React, { useEffect } from "react";
import { useRootStoreContext } from "../../store/rootStore";
import * as Yup from "yup";
import "./modal-window.css";
import { useFormik } from "formik";
import { validation } from "../../utils/validation";

const ModalWindow = observer(() => {
  const context = useRootStoreContext();
  const modalStore = context.modalStore;
  const cardsStore = context.cardsStore;

  useEffect(() => {
    modalStore.getInfoImage();
  }, [modalStore]);

  const formik = useFormik({
    initialValues: {
      name: "",
      text: "",
      id: cardsStore.currentId,
    },
    validationSchema: Yup.object(validation),
    onSubmit: (data, { resetForm }) => {
      modalStore.setComments([...modalStore.comments, data]);
      resetForm({});
    },
  });
  const dateFormat = (date: number): string => {
    const d = new Date(date);
    return d.toLocaleDateString();
  };

  return (
    <div className="background-modal">
      {cardsStore.loader ? (
        <div>Loading...</div>
      ) : (
        <div className="modal-wrapper">
          <div>
            <img
              className="image"
              src={modalStore.imageData && modalStore.imageData.url}
              alt="items"
            />
            <form onSubmit={formik.handleSubmit}>
              <div className="form-group mt-4 margin-bottom">
                <input
                  type="name"
                  className="form-control"
                  id="name"
                  name="name"
                  placeholder="Enter your name"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                />
              </div>
              {formik.errors.name && formik.touched.name && (
                <div className="text-danger margin-top">Invalid name!</div>
              )}
              <div className="input-group">
                <textarea
                  className="form-control"
                  aria-label="With textarea"
                  id="text"
                  name="text"
                  placeholder="Enter your comment"
                  value={formik.values.text}
                  onChange={formik.handleChange}
                />
              </div>
              {formik.errors.text && formik.touched.text && (
                <div className="text-danger mt-1 margin-style ">
                  Invalid comment!
                </div>
              )}
              <button
                type="submit"
                className="btn btn-primary button-size margin-custom"
              >
                Submit
              </button>
            </form>
          </div>
          <div className="comments">
            {modalStore.imageData?.comments.length !== 0 &&
              modalStore.imageData?.comments.map((item) => (
                <div className="comment-wrapper mb-2">
                  <div className="data-section">{dateFormat(item.date)}</div>
                  <div className="text-section">{item.text}</div>
                </div>
              ))}

            {modalStore.comments?.map(
              (item) =>
                item.id === cardsStore.currentId && (
                  <div className="comment-wrapper mb-2">
                    <div className="data-section">
                      {dateFormat(Date.parse(String(new Date())))}
                    </div>
                    <div className="text-section">{item.text}</div>
                  </div>
                )
            )}
            {modalStore.imageData?.comments.length === 0 &&
              modalStore.existComment.length === 0 && (
                <div className="comment-wrapper">No comments yet</div>
              )}
          </div>
          <div
            className="close close-button"
            onClick={() => modalStore.setModal(false)}
          >
            x
          </div>
        </div>
      )}
    </div>
  );
});

export default ModalWindow;
