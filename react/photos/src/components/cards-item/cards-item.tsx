import { observer } from "mobx-react";
import React, { useEffect } from "react";
import { useRootStoreContext } from "../../store/rootStore";
import "./cards-item.css";

const CardsItem = observer(() => {
  const context = useRootStoreContext();
  const cardsStore = context.cardsStore;
  const modalStore = context.modalStore;

  useEffect(() => {
    cardsStore.getCards();
  }, [cardsStore]);

  return (
    <div className="cards-wrapper">
      {cardsStore.cards.map((item) => (
        <img
          className="images"
          src={item.url}
          alt="content"
          key={item.id}
          onClick={() => {
            cardsStore.setCurrentId(item.id);
            modalStore.setModal(true);
            cardsStore.setLoader(true);
            console.log(cardsStore.loader);
          }}
        />
      ))}
    </div>
  );
});

export default CardsItem;
