import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/app';
import reportWebVitals from './reportWebVitals';
import { rootStore, RootStoreContext } from "./store/rootStore";


ReactDOM.render(
  <RootStoreContext.Provider value={rootStore}>
    <App />
  </RootStoreContext.Provider>,
  document.getElementById("root")
);

reportWebVitals();