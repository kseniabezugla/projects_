import React, { useEffect, useState } from "react";

import { Link, useParams } from "react-router-dom";
import { getData } from "../../services/requests";
import { Starship } from "../../utils/interfaces";

const Starships = () => {
  let param: { id?: string | undefined } = useParams();
  const [currentStarships, setCurrentStarships] =
    useState<Starship | null>(null);

  useEffect(() => {
    getData(`https://swapi.dev/api/starships/${param?.id}`).then((response) =>
      setCurrentStarships(response)
    );
  }, [param?.id]);
  return (
    <div className="wrapper">
      <div className="container-return">
        <Link to="/" className="return">
          Return
        </Link>
      </div>
      <div className="container">
        <div>Starship Information</div>
        <div className="line"></div>
        <div className="info-wrapper">
          <div>Name: {currentStarships?.name}</div>
          <div>Length: {currentStarships?.length}</div>
          <div>Manufacturer: {currentStarships?.manufacturer}</div>
          <div>
            Max atmosphering speed: {currentStarships?.max_atmosphering_speed}
          </div>
          <div>Passengers: {currentStarships?.passengers}</div>
        </div>
      </div>
    </div>
  );
};
export default Starships;
