import React, { useEffect, useState } from "react";

import { Link, useParams } from "react-router-dom";
import { getData } from "../../services/requests";
import { Vehicle } from "../../utils/interfaces";

const Vehicles = () => {
  const [currentVehicles, setCurrentVehicles] = useState<Vehicle | null>(null);
  let param: { id?: string | undefined } = useParams();

  useEffect(() => {
    getData(`https://swapi.dev/api/vehicles/${param?.id}`).then((response) =>
      setCurrentVehicles(response)
    );
  }, [param?.id]);

  return (
    <div className="wrapper">
      <div className="container-return">
        <Link to="/" className="return">
          Return
        </Link>
      </div>
      <div className="container">
        <div>Vehicle Information</div>
        <div className="line"></div>
        <div className="info-wrapper">
          <div>Name: {currentVehicles?.name}</div>
          <div>Length: {currentVehicles?.length}</div>
          <div>Manufacturer: {currentVehicles?.manufacturer}</div>
          <div>
            Max atmosphering speed: {currentVehicles?.max_atmosphering_speed}
          </div>
          <div>Passengers: {currentVehicles?.passengers}</div>
        </div>
      </div>
    </div>
  );
};
export default Vehicles;
