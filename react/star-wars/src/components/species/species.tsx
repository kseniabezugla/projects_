import React, { useEffect, useState } from "react";
import "./species.css";
import { Link, useParams } from "react-router-dom";
import { getData } from "../../services/requests";
import { Film, Specie } from "../../utils/interfaces";
import { useFilms } from "../../hooks/useFilms";
import { getPath } from "../../utils/functions";

const Species = () => {
  let param: { id?: string | undefined } = useParams();
  const [currentSpecies, setCurrentSpecies] = useState<Specie | null>(null);
  const [films] = useFilms(currentSpecies?.films);

  useEffect(() => {
    getData(`https://swapi.dev/api/species/${param?.id}`).then((response) =>
      setCurrentSpecies(response)
    );
  }, [param?.id]);

  return (
    <div className="wrapper">
      <div className="container-return">
        <Link to="/" className="return species-return">
          Return
        </Link>
      </div>
      <div className="container">
        <div>Species Information</div>
        <div className="line"></div>
        <div className="info-wrapper">
          <div>Name: {currentSpecies?.name}</div>
          <div>Average height: {currentSpecies?.average_height}</div>
          <div>Classification: {currentSpecies?.classification}</div>
          <div>Language: {currentSpecies?.language}</div>
          <div>Eye colors: {currentSpecies?.eye_colors}</div>
          <div>Skin colors: {currentSpecies?.skin_colors}</div>
          {films?.length !== 0 && (
            <div>
              Films:
              <ul>
                {films?.map((item: Film) => (
                  <li>
                    <Link to={`${getPath(item.url)}`} className="color-link">
                      {item?.title}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default Species;
