import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Films from "../films";
import PeopleInfo from "../people-info";
import Species from "../species";
import Starships from "../starships";
import Vehicles from "../vehicles";
import "./App.css";

const App = () => {
  return (
    <div className="App">
      <div className="App-header">
        <Router>
          <Switch>
            <Route exact path="/">
              <PeopleInfo />
            </Route>
            <Route path="/films/:id">
              <Films />
            </Route>
            <Route path="/species/:id">
              <Species />
            </Route>
            <Route path="/starships/:id">
              <Starships />
            </Route>
            <Route path="/vehicles/:id">
              <Vehicles />
            </Route>
          </Switch>
        </Router>
      </div>
    </div>
  );
};

export default App;
