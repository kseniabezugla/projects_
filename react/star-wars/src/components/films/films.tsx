import React, { useEffect, useState } from "react";

import { Link, useParams } from "react-router-dom";
import { useSpecies } from "../../hooks/useSpecies";
import { getData } from "../../services/requests";
import { getPath } from "../../utils/functions";
import { Film, Specie } from "../../utils/interfaces";
import "./films.css";

const Films = () => {
  let param: { id?: string | undefined } = useParams();
  const [currentFilm, setCurrentFilm] = useState<Film | null>(null);
  const [species] = useSpecies(currentFilm?.species);
  const [starships] = useSpecies(currentFilm?.starships);

  useEffect(() => {
    getData(`https://swapi.dev/api/films/${param?.id}`).then((response) =>
      setCurrentFilm(response)
    );
  }, [param?.id]);

  return (
    <div className="wrapper">
      <div className="container-return">
        <Link to="/" className="return">
          Return
        </Link>
      </div>
      <div className="container">
        <div>Film Information</div>
        <div className="line"></div>
        <div className="info-wrapper">
          <div>Title: {currentFilm?.title}</div>
          <div>Director: {currentFilm?.director}</div>
          {species?.length !== 0 && (
            <div>
              Species:
              <ul className="size-items">
                {species?.map((item: Specie) => (
                  <li>
                    <Link to={`${getPath(item.url)}`} className="color-link ">
                      {item.name}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          )}
          {starships?.length !== 0 && (
            <div>
              Starships:
              <ul className="size-items">
                {starships?.map((item) => (
                  <li>
                    <Link to={`${getPath(item.url)}`} className="color-link">
                      {item.name}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default Films;
