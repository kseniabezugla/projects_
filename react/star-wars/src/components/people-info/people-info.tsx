import React from "react";
import "./people-info.css";
import { Link } from "react-router-dom";
import { Film, Specie, Starship, Vehicle } from "../../utils/interfaces";
import { getPath } from "../../utils/functions";
import { usePeople } from "../../hooks/usePeople";
import { useFilms } from "../../hooks/useFilms";
import { useSpecies } from "../../hooks/useSpecies";
import { useStarships } from "../../hooks/useStarships";
import { useVehicles } from "../../hooks/useVehicles";

const PeopleInfo = () => {
  const [people] = usePeople();
  const [films, loader] = useFilms(people?.films);
  const [species] = useSpecies(people?.species);
  const [starships] = useStarships(people?.starships);
  const [vehicles] = useVehicles(people);

  return loader ? (
    <div>Loading...</div>
  ) : (
    <div className="container">
      <div>People Information</div>
      <div className="line"></div>
      <div className="info-wrapper">
        <div>Name: {people?.name}</div>
        <div>Gender: {people?.gender}</div>
        {!loader && films && (
          <div>
            Films:
            <ul>
              {films.map((item: Film) => (
                <li>
                  <Link to={`${getPath(item.url)}`} className="color-link">
                    {item.title}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        )}
        <div>Height: {people?.height}</div>
        <div>Skin color: {people?.skin_color}</div>
        {species?.length !== 0 && (
          <div>
            Species:
            <ul>
              {species?.map((item: Specie) => (
                <li>
                  <Link to={`${getPath(item.url)}`} className="color-link">
                    {item.name}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        )}
        {starships?.length !== 0 && (
          <div>
            Starships:
            <ul>
              {starships?.map((item: Starship) => (
                <li>
                  <Link to={`${getPath(item.url)}`} className="color-link">
                    {item.name}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        )}
        {vehicles?.length !== 0 && (
          <div>
            Vehicles:
            <ul>
              {vehicles?.map((item: Vehicle) => (
                <li>
                  <Link to={`${getPath(item.url)}`} className="color-link">
                    {item.name}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    </div>
  );
};
export default PeopleInfo;
