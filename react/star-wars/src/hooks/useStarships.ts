import { getData } from "./../services/requests";
import { Starship } from "../utils/interfaces";
import { useEffect } from "react";
import { useState } from "react";

export function useStarships(
  starshipUrls: string[] | undefined
): [Starship[] | null] {
  const [starships, setStarships] = useState<Starship[]>([]);

  useEffect(() => {
    if (starshipUrls) {
      starshipUrls.forEach((item) =>
        getData(item).then((response) => {
          setStarships((starship) => [...starship, response]);
        })
      );
    }
  }, [starshipUrls]);

  return [starships];
}
