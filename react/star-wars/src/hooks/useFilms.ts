import { Film } from "../utils/interfaces";
import { useEffect } from "react";
import { useState } from "react";
import axios from "axios";

export function useFilms(filmsUrls?: string[]): [Film[] | null, boolean] {
  const [films, setFilms] = useState<Film[]>([]);
  const [loader, setLoader] = useState(true);

  const getFilms = async (data: string) => {
    try {
      const response = await axios.get(data);
      setLoader(false);
      console.log(response.data);
      return response.data;
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (filmsUrls) {
      filmsUrls.forEach((item) =>
        getFilms(item).then((response) => {
          setFilms((film) => [...film, response]);
        })
      );
    }
  }, [filmsUrls]);

  return [films, loader];
}
