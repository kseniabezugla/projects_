import { People } from "../utils/interfaces";
import { useEffect } from "react";
import { useState } from "react";
import { instance } from "../services/requests";

export function usePeople(): [People | null] {
  const [people, setPeople] = useState<People | null>(null);

  const getPeople = async (
    people: number = Math.floor(Math.random() * 83) + 1
  ) => {
    try {
      const response = await instance.get(
        `people/${people === 17 ? people + 1 : people}/`
      );
      console.log(response.data)
      setPeople(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getPeople().then(() => setPeople);
  }, []);

  return [people];
}
