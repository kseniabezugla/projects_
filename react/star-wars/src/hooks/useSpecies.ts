import { getData } from "./../services/requests";
import { Specie } from "./../utils/interfaces";
import { useEffect } from "react";
import { useState } from "react";

export function useSpecies(SpeciesUrl?: string[]): [Specie[] | null] {
  const [species, setSpecies] = useState<Specie[]>([]);

  useEffect(() => {
    if (SpeciesUrl) {
      SpeciesUrl.forEach((item) =>
        getData(item).then((response) => {
          setSpecies((species) => [...species, response]);
        })
      );
    }
  }, [SpeciesUrl]);

  return [species];
}
