import { getData } from "../services/requests";
import { People, Vehicle } from "../utils/interfaces";
import { useEffect } from "react";
import { useState } from "react";

export function useVehicles(people: People | null): [Vehicle[] | null] {
  const [vehicles, setVehicles] = useState<Vehicle[]>([]);

  useEffect(() => {
    if (people) {
      people.vehicles.forEach((item) =>
        getData(item).then((response) => {
          setVehicles((vehicle) => [...vehicle, response]);
        })
      );
    }
  }, [people]);

  return [vehicles];
}
