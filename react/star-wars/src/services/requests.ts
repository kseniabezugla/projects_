import axios from "axios";

export const instance = axios.create({
  baseURL: "https://swapi.dev/api/",
});

export const getData = async (data: string) => {
  try {
    const response = await axios.get(data);
    console.log(response.data);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};
