import React, { useState, useEffect } from "react";
import ChoiceSectionItem from "../choice-section-item";
import AviaCurrencyFilter from "../avia-currency-filter";
import AviaListInfo from "../avia-list-info";
import avia from "../../assets/images/avia.png";
import "../../index.css";
import "./app.css";
import { allStops } from "../../utils/AviaVariables";

const App = () => {
  const [tickets, setTickets] = useState([]);
  const [currency, setCurrency] = useState("UAH");
  const [filter, setFilter] = useState(allStops);

  useEffect(() => {
    fetch("./tickets.json")
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        setTickets(data.tickets);
      });
  }, []);

  return (
    <div className="app bg-light">
      <img className="img-logo" src={avia} alt="avia" />
      <div className="d-flex justify-content-between">
        <div className="elements-border ">
          <AviaCurrencyFilter setCurrency={setCurrency} currency={currency} />
          <ChoiceSectionItem filter={filter} setFilter={setFilter} />
        </div>
        <div className="d-flex row size-wrapper ">
          {tickets
            .filter((item) => filter.includes(item.stops))
            .map((ticket, index) => (
              <AviaListInfo key={index} ticket={ticket} currency={currency} />
            ))}
        </div>
      </div>
    </div>
  );
};
export default App;
