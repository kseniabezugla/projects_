import React from "react";
import "./modal-window.css";
import InputData from "../input-data";
import swal from "sweetalert";
import * as Yup from "yup";
import { useFormik } from "formik";

const ModalWindow = ({ closeModal }) => {
  const formik = useFormik({
    initialValues: {
      gender: "",
      firstName: "",
      lastName: "",
      email: "",
      number_passport: "",
      date_of_birth: "",
      citizenship: "",
      phone_number: "",
    },
    validationSchema: Yup.object({
      firstName: Yup.string()
        .min(2, "Слишком короткое имя!")
        .max(50, "Слишком длинное имя!")
        .required("Обязательное поле для заполнения"),
      lastName: Yup.string()
        .min(2, "Слишком короткая фамилия!")
        .max(50, "Слишком длинная фамилия!")
        .required("Обязательное поле для заполнения"),
      email: Yup.string()
        .email("Неверный email!")
        .required("Обязательное поле для заполнения"),
      number_passport: Yup.string()
        .min(6, "Проверьте правильность ввода, мало данных!")
        .required("Обязательное поле для заполнения"),
      citizenship: Yup.string()
        .min(2, "Слишком мало символов!")
        .max(20, "Слишком длинн!")
        .required("Обязательное поле для заполнения"),
      phone_number: Yup.string()
        .min(10, "Недостаточно данных")
        .max(13, "Слишком длинный номер телефона!")
        .required("Обязательное поле для заполнения"),
    }),
    onSubmit: () => {
      swal({
        icon: "success",
        button: "закрыть",
        text: "Заявка успешно оформлена",
      });
    },
  });

  return (
    <div className="background-modal">
      <div className="modal-wrapper ">
        <h2 className="text-uppercase title-font "> Бронирование билета</h2>
        <div>
          <form
            onSubmit={(event) => {
              formik.handleSubmit(event);
            }}
          >
            <InputData
              id="firstName"
              name="firstName"
              placeholder="Ivan"
              text="Имя"
              error={formik.errors.firstName}
              touched={formik.touched.firstName}
              value={formik.values.firstName}
              handleChange={formik.handleChange}
            />
            <div className="d-flex justify-content-between margin-add">
              <label>Выберите пол:</label>
              <select
                className="rounded"
                id="gender"
                name="gender"
                value={formik.values.gender}
                onChange={formik.handleChange}
              >
                <option>Мужской</option>
                <option>Женский</option>
              </select>
            </div>
            <InputData
              id="lastName"
              name="lastName"
              placeholder="Petrov"
              text="Фамилия"
              error={formik.errors.lastName}
              touched={formik.touched.lastName}
              value={formik.values.lastName}
              handleChange={formik.handleChange}
            />
            <InputData
              id="citizenship"
              name="citizenship"
              placeholder="Ukraine"
              text="Гражданство"
              error={formik.errors.citizenship}
              touched={formik.touched.citizenship}
              value={formik.values.citizenship}
              handleChange={formik.handleChange}
            />
            <InputData
              id="number_passport"
              name="number_passport"
              placeholder="RS123456"
              text="Серия и № документа"
              error={formik.errors.number_passport}
              touched={formik.touched.number_passport}
              value={formik.values.number_passport}
              handleChange={formik.handleChange}
            />
            <InputData
              id="phone_number"
              name="phone_number"
              placeholder="+38 0XX XXX XX XX"
              text="Номер телефона"
              error={formik.errors.phone_number}
              touched={formik.touched.phone_number}
              value={formik.values.phone_number}
              handleChange={formik.handleChange}
            />
            <InputData
              id="email"
              name="email"
              placeholder="example@gmail.com"
              text="Email"
              error={formik.errors.email}
              touched={formik.touched.email}
              value={formik.values.email}
              handleChange={formik.handleChange}
            />

            <div className="d-flex justify-content-between ">
              <button
                onClick={() => closeModal(false)}
                className="btn btn-primary"
                type="close"
              >
                Закрыть
              </button>
              <button className="btn btn-primary" type="submit">
                Подтвердить
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalWindow;
