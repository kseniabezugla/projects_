import React, { useState } from "react";
import Moment from "react-moment";
import turkiskh from "../../assets/images/turkish.png";
import ModalWindow from "../modal-window";
import "moment/locale/ru";
import { convertCurrency, formatStops } from "../../utils/AviaFunctions";

const AviaListInfo = ({ ticket, currency }) => {
  const {
    origin,
    origin_name,
    destination,
    destination_name,
    departure_date,
    departure_time,
    arrival_date,
    arrival_time,
    stops,
    price,
  } = ticket;

  const [isModalShow, setIsModalShow] = useState(false);
  if (isModalShow) return <ModalWindow closeModal={setIsModalShow} />;

  return (
    <div className=" d-flex row border-element rounded-lg">
      <div className="wrapper">
        <img src={turkiskh} className="img-turkish " alt="turkish airlines" />
        <button
          type="button"
          className="btn btn-danger"
          onClick={() => setIsModalShow(true)}
        >
          <span className="text-uppercase">Price </span>
          <span> {convertCurrency(price, currency)} </span>
        </button>
      </div>
      <div className="d-flex row between-values">
        <div className="between-values">
          <span className="travel_time">{departure_time}</span>
          <br />
          <span className="travel_text"> {origin}, </span>
          <span className="travel_text">{origin_name}</span>
          <br />
          <span className="travel_date">
            <Moment format="D MMM YYYY, dd" locale="en">
              {departure_date}
            </Moment>
          </span>
        </div>
        <div className="between-values">
          <span className="travel_date stops text-uppercase">
            {formatStops(stops)}
          </span>
          <div className="plane"></div>
        </div>
        <div className="between-values">
          <span className="travel_time">{arrival_time}</span>
          <br />
          <span className="travel_text">{destination_name},</span>
          <span className="travel_text"> {destination}</span>

          <br />
          <span className="travel_date">
            <Moment format="D MMM YYYY, dd" locale="en">
              {arrival_date}
            </Moment>
          </span>
        </div>
      </div>
    </div>
  );
};
export default AviaListInfo;
