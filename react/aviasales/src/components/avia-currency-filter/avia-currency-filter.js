import React from "react";
import { currencyButtons } from "../../utils/AviaVariables";

const AviaCurrencyFilter = ({ setCurrency, currency }) => {
  return (
    <div>
      <p className="text-uppercase avia-font">Currency</p>
      <div className="btn-group btn-group-lg margin-b" role="group">
        {currencyButtons.map(({ currencyName }) => {
          return (
            <button
              key={currencyName}
              type="button"
              className={`btn ${
                currency === currencyName
                  ? "btn-primary"
                  : "btn-outline-primary"
              }`}
              onClick={() => setCurrency(currencyName)}
            >
              {currencyName}
            </button>
          );
        })}
      </div>
    </div>
  );
};

export default AviaCurrencyFilter;
