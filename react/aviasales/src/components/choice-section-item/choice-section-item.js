import React from "react";
import { allStops, checkBoxFlightData } from "../../utils/AviaVariables";
import "./choice-section-item.css";
const ChoiceSectionItem = ({ filter, setFilter }) => {
  const onAllChange = () => {
    setFilter(filter.length === allStops.length ? [] : allStops);
  };
  const onChange = (e) => {
    const { value } = e.target;
    const checked = filter.includes(value);
    const newFilter = checked
      ? filter.filter((item) => item !== value)
      : [...filter, value];
    setFilter(newFilter);
  };

  return (
    <div>
      <p className="text-uppercase avia-font">stopovers number</p>
      <div className="custom-control custom-checkbox ">
        <input
          onChange={onAllChange}
          type="checkbox"
          className="custom-control-input"
          id={0}
          value={"All"}
          checked={filter.length === allStops.length}
        />
        <label className="custom-control-label" htmlFor={0}>
          All
        </label>
      </div>
      {checkBoxFlightData.map(({ id, text, value }) => {
        return (
          <div className="custom-control custom-checkbox ">
            <input
              onChange={onChange}
              type="checkbox"
              className="custom-control-input"
              id={id}
              value={value}
              checked={filter.includes(value)}
            />
            <label className="custom-control-label" htmlFor={id}>
              {text}
            </label>
          </div>
        );
      })}
    </div>
  );
};
export default ChoiceSectionItem;
