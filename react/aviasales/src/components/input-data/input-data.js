import React from "react";
import "./input-data.css";

const InputData = ({
  id,
  name,
  placeholder,
  text,
  error,
  touched,
  value,
  handleChange,
}) => {
  return (
    <div key={id}>
      <div className="d-flex justify-content-between margin-add">
        <label htmlFor="firstName">{text}</label>
        <input
          className="rounded"
          type="text"
          id={id}
          name={name}
          placeholder={placeholder}
          value={value}
          onChange={handleChange}
        />
      </div>
      {error && touched && <p className="error-text">{error}</p>}
    </div>
  );
};
export default InputData;
