export function convertCurrency(price: number, currency: string) {
  const usd = 35.86,
    eur = 36.3;

  switch (currency) {
    case "UAH":
      return price + " ₴";
    case "USD":
      return +(price / usd).toFixed(2) + " $";
    case "EUR":
      return +(price / eur).toFixed(2) + " €";
  }
}
export const formatStops = (stops: string) => {
  switch (stops) {
    case "zero":
      return "direct flight";
    case "one":
      return "1 stopover";
    case "two":
      return "2 stopovers";
    case "three":
      return "3 stopovers";
  }
};
