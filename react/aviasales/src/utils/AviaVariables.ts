export const currencyButtons = [
  { name: "UAH", currencyName: "UAH" },
  { name: "USD", currencyName: "USD" },
  { name: "EUR", currencyName: "EUR" },
];

export const checkBoxFlightData = [
  //   { id: 0, text: "All", value: "all" },
  { id: 1, text: "Direct flight", value: "zero" },
  { id: 2, text: "1 stopover", value: "one" },
  { id: 3, text: "2 stopovers", value: "two" },
  { id: 4, text: "3 stopovers", value: "three" },
];
export const allStops = ["zero", "one", "two", "three"];
