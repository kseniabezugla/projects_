import React from "react";
import "./properties.css";

const Properties = () => {
  return (
    <div className="d-block rounded border border-light pl-3 width">
      <div className="d-flex ">
        <i className="fas fa-list-ul fa-lg mt-3 mr-2"></i>
        <h3 className=" text-uppercase mt-1">Properties</h3>
      </div>     
    </div>
  );
};
export default Properties;
