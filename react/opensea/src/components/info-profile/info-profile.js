import React from "react";
import "./info-profile.css";

const InfoProfile = ({name, description}) => {
  return (
    <div className="d-block rounded  pl-3 size">
     <h2 className="text-uppercase pb-4">{name}</h2>
     <p >{description}</p>
    </div>
  );
};
export default InfoProfile;
