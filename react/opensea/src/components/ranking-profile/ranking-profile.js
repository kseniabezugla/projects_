import React from "react";
import { ProgressBar } from "react-bootstrap";
import "./ranking-profile.css";

const RankingProfile = ({ attributes }) => {
  return (
    <div className="d-block rounded border border-light pl-3 width">
      <div className="d-flex ">
        <i className="fas fa-chart-bar fa-lg mt-3 mr-2"></i>
        <h3 className=" text-uppercase mt-1">Ranking</h3>
      </div>
      <hr />
      {attributes.map((item) => (
        <div className="mb-4 mr-2" key={item}>
          {item.trait_type}
          <ProgressBar now={item.value} />
        </div>
      ))}
    </div>
  );
};
export default RankingProfile;
