import React, { useEffect, useState } from "react";
import ImageProfile from "../image-profile/image-profile";
import InfoProfile from "../info-profile/info-profile";
import Properties from "../properties/properties";
import RankingProfile from "../ranking-profile/ranking-profile";
import "./app.css";

const App = () => {
  const [info, setInfo] = useState({});
  useEffect(() => {
    fetch("./openSea.json")
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        setInfo(data);
      });
  }, []);

  return (
    <div className="app ">
      <div className=" d-flex mt-4 mb-3">
        <ImageProfile key={info.name} image={info.image} />
        <InfoProfile
          key={info.name}
          name={info.name}
          description={info.description}
        />
      </div>
      <div className="d-flex ">
        <Properties />
        <div>
          {info.attributes && (
            <RankingProfile key={info.name} attributes={info.attributes} />
          )}
        </div>
      </div>
    </div>
  );
};
export default App;
