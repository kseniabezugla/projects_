import React from "react";
import "./image-profile.css";

const ImageProfile = ({image}) => {
  return (
    <div className="d-block mr-3 ">
      <img className="rounded border border-light" src={image} alt="cat" />
    </div>
  );
};
export default ImageProfile;
