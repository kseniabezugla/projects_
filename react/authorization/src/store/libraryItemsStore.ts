import axios from "axios";
import { Activity } from "./library-interface/activity";
import { ILibrary } from "./library-interface/Ilibrary";
import { makeObservable, observable } from "mobx";

class LibraryItemsStore {
  cards: ILibrary[] = [];
  loading = false;
  rootStore: any;

  constructor(rootStore: any) {
    makeObservable(this, {
      cards: observable,
      loading: observable,
    });
    this.rootStore = rootStore;
  }
  setLoading = (newLoading: boolean) => {
    this.loading = newLoading;
  };
  setCards(newCards: ILibrary[]) {
    this.cards = newCards;
  }

  getData = async () => {
    this.setLoading(true);
    try {
      const response = await axios.get(
        "https://wlapi.goally.co/v1/api/routines/library",
        {
          headers: {
            Authorization: this.rootStore.loginFormStore.token,
            "Content-type": "application/json",
          },
        }
      );
      this.setCards(response.data);
      this.setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };
  calcTime = (item: ILibrary, completionTime: keyof Activity) => {
    return item.activities
      .map((item: Activity) => {
        return item[completionTime];
      })
      .reduce((sum: number, current: number) => {
        return sum + current;
      });
  };
}
export default LibraryItemsStore;
