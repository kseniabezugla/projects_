import axios from "axios";
import { makeObservable, observable, action } from "mobx";

class LoginFormStore {
  token = localStorage.getItem("token");
  isPassword = false;
  errors = "";
  loading = false;
  login = false;
  rootStore: any;

  constructor(rootStore: any) {
    makeObservable(this, {
      token: observable,
      login: observable,
      isPassword: observable,
      errors: observable,
      togglePasswordVisibility: action.bound,
      loading: observable,
    });
    this.rootStore = rootStore;
  }
  setLoading = (newLoading: boolean) => {
    this.loading = newLoading;
  };
  setErrors = (newError: string) => {
    this.errors = newError;
  };

  setIsPassword = (newIsPassword: boolean) => {
    this.isPassword = newIsPassword;
  };

  togglePasswordVisibility() {
    this.setIsPassword(!this.isPassword);
  }

  setToken = (newToken: string) => {
    this.token = newToken;
  };
  setLogin = (newLogin: boolean) => {
    this.login = newLogin;
  };

  sendData = async (dataForm: { email: string; password: string }) => {
    this.setLoading(true);
    try {
      const response = await axios.post(
        "https://wlapi.goally.co/v1/api/auth/login",
        dataForm
      );
      const token = response.data.token;
      localStorage.setItem("token", token);
      this.setToken(localStorage.getItem("token")!);
    } catch (error) {
      this.setErrors(error.response.data.message);
      throw new Error(error);
    } finally {
      this.setLoading(false);
      this.setLogin(true);
    }
  };
}
export default LoginFormStore;
