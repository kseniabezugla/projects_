export interface Activity {
  migrated: boolean;
  allowPush: boolean;
  allowPauseActivity: boolean;
  parentActivityId?: any;
  imgURL?: string | string;
  audioUrl?: string | string;
  _id: string;
  name: string;
  minCompletionTime: number;
  maxCompletionTime: number;
  libraryType: string;
  allowCancelActivity: boolean;
  showTimer: boolean;
  ordering: number;
  createdBy: string;
  createdAt: string;
  updatedAt: string;
}
