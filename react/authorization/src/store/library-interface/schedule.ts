export interface Schedule {
  Mon: string;
  Thu?: string;
  Wed?: string;
  Tue?: string;
  Fri?: string;
  Sat?: string;
  Sun?: string;
}
