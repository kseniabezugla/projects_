import { Schedule } from "./schedule";
import { Activity } from "./activity";

export interface ILibrary {
  _id: string;
  showOnLearnerApp: boolean;
  allowToOverride: boolean;
  allowClientToCancel: boolean;
  migrated?: boolean;
  parentRoutineId?: any;
  name: string;
  numberOfPointsOnTime: number;
  numberOfPointsLate: number;
  type: string;
  imgURL: string;
  activities: Activity[];
  clientId?: any;
  schedule?: Schedule;
  libraryType: string;
  createdBy: string;
  createdAt: string;
  updatedAt: string;
  __v: number;
  ordering: number;
}
