import { createContext, useContext } from "react";
import LibraryItemsStore from "./libraryItemsStore";
import LoginFormStore from "./loginFormStore";

class RootStore {
  constructor() {
    this.loginFormStore = new LoginFormStore(this);
    this.libraryItemsStore = new LibraryItemsStore(this);
  }
  loginFormStore: LoginFormStore;
  libraryItemsStore: LibraryItemsStore;
}

export const rootStore = new RootStore();

export const RootStoreContext = createContext<RootStore>({} as RootStore);

export const useRootStoreContext = () => useContext(RootStoreContext);
