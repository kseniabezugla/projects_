import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./components/app/app";
import reportWebVitals from "./reportWebVitals";
import { rootStore, RootStoreContext } from "./store/rootStore";
import { BrowserRouter } from "react-router-dom";

ReactDOM.render(
  <RootStoreContext.Provider value={rootStore}>
    <BrowserRouter>
    <App />
    </BrowserRouter>
  </RootStoreContext.Provider>,
  document.getElementById("root")
);

reportWebVitals();
