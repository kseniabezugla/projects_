import * as Yup from "yup";

export const validation = {
  email: Yup.string()
    .required("Please Enter your email")
    .matches(/^.([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/),
  password: Yup.string()
    .required("Please Enter your password")
    .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,}$/),
};
