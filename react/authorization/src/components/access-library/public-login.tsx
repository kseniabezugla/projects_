import { observer } from "mobx-react";
import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useRootStoreContext } from "../../store/rootStore";

const PublicRoot = observer(
  ({ component: Component, restricted, ...rest }: any) => {
    const context = useRootStoreContext();
    const loginFormStore = context.loginFormStore;
    return (
      <Route
        {...rest}
        render={(props) =>
          loginFormStore.login && restricted ? (
            <Redirect to="/library-items" />
          ) : (
            <Component {...props} />
          )
        }
      />
    );
  }
);

export default PublicRoot;
