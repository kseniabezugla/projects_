import { observer } from "mobx-react";
import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useRootStoreContext } from "../../store/rootStore";

const PrivateRoot = observer(({ component: Component, ...rest }: any) => {
  const context = useRootStoreContext();
  const loginFormStore = context.loginFormStore;
  return (
    <Route
      {...rest}
      render={(props) =>
        loginFormStore.login ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );
});

export default PrivateRoot;
