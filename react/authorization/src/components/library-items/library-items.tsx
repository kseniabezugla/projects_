import { observer } from "mobx-react";
import React, { useEffect } from "react";
import { useRootStoreContext } from "../../store/rootStore";
import Loader from "../loader/loader";
import "./library-items.css";

const LibraryItems = observer(() => {
  const context = useRootStoreContext();
  const libraryItemsStore = context.libraryItemsStore;

  useEffect(() => {
    libraryItemsStore.getData();
  }, [libraryItemsStore]);

  return libraryItemsStore.loading ? (
    <div className="width-position">
      <Loader />
    </div>
  ) : (
    <div className="wrapper flex-column mt-5">
      <h2>Goally Template Library</h2>
      <div className=" wrapper-cards d-flex flex-column rounded scroll-items scrollbar-primary">
        {libraryItemsStore.cards.map((item) => {
          return (
            <div className="cards d-flex flex-column rounded" key={item._id}>
              <div className="d-flex row">
                <img
                  className="mr-3 img-size"
                  src={item.imgURL}
                  alt="library"
                />
                <div>
                  <h4>{item.name}</h4>
                  <div className="d-flex text-secondary ">
                    <i className="far fa-clock mt-1 mr-1"></i>
                    <p>
                      min{" "}
                      {libraryItemsStore.calcTime(item, "minCompletionTime")}{" "}
                      minutes - max{" "}
                      {libraryItemsStore.calcTime(item, "maxCompletionTime")}{" "}
                      minutes
                    </p>
                  </div>
                </div>
              </div>
              <div className="border border-light mt-4 d-flex wigth-line"></div>
            </div>
          );
        })}
      </div>
    </div>
  );
});
export default LibraryItems;
