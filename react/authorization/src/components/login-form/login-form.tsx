import { useFormik } from "formik";
import { observer } from "mobx-react";
import React from "react";
import { useRootStoreContext } from "../../store/rootStore";
import * as Yup from "yup";
import { validation } from "../../utils/validation";
import "./login-form.css";
import Loader from "../loader/loader";

const LoginForm = observer(({ history }: any) => {
  const context = useRootStoreContext();
  const loginFormStore = context.loginFormStore;

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object(validation),
    onSubmit: async (data) => {
      try {
        await loginFormStore.sendData(data);
        history.push("/library-items");
      } catch (e) {
        console.log(e);
      }
    },
  });
  return (
    <div>
      <div className="wrapper position-form">
        {loginFormStore.errors ? (
          <div className="d-flex justify-content-center error rounded">
            <i className="fas fa-exclamation-circle mt-1 mr-1"></i>
            {loginFormStore.errors}
          </div>
        ) : null}
      </div>
      <div className="wrapper">
        <div className="from-size">
          <h2 className="mb-3"> Log in</h2>
          <form
            onSubmit={(event) => {
              formik.handleSubmit(event);
            }}
          >
            <div className="form-group">
              <input
                type="email"
                className="form-control-lg"
                id="email"
                name="email"
                placeholder="Email"
                onChange={formik.handleChange}
              />
            </div>
            {formik.errors.email && formik.touched.email && (
              <p className="text-danger mb-3">Invalid email!</p>
            )}
            <div className="form-group d-flex align-items-center justify-content-end">
              <input
                type={loginFormStore.isPassword ? "text" : "password"}
                className="form-control-lg position-absolute"
                id="password"
                name="password"
                placeholder="Password"
                onChange={formik.handleChange}
              />
              <i
                className={`far  eye p-3 ${
                  loginFormStore.isPassword ? "fa-eye" : "fa-eye-slash"
                }`}
                onClick={loginFormStore.togglePasswordVisibility}
              ></i>
            </div>
            {formik.errors.password && formik.touched.password && (
              <p className="text-danger mb-3">Invalid password!</p>
            )}{" "}
            {loginFormStore.loading ? (
              <Loader />
            ) : (
              <button
                type="submit"
                className="btn-lg btn-primary button-size mt-4"
                disabled={loginFormStore.loading}
              >
                Log in
              </button>
            )}
          </form>
        </div>
      </div>
    </div>
  );
});
export default LoginForm;
