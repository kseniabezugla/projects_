import { observer } from "mobx-react";
import React from "react";

const Loader = observer(() => {
  return (
    <div className="d-flex justify-content-center align-items-center">
      <button className="btn btn-primary" type="button" disabled>
        <span
          className="spinner-border spinner-border-sm mr-1"
          role="status"
          aria-hidden="true"
        ></span>
        Loading...
      </button>
    </div>
  );
});

export default Loader;
