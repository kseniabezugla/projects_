import React from "react";
import LibraryItems from "../library-items/library-items";
import LoginForm from "../login-form/login-form";
import { Switch } from "react-router-dom";
import PrivateRoot from "../access-library/private-library";
import PublicRoot from "../access-library/public-login";

const App = () => {
  return (
    <main>
      <Switch>
        <PublicRoot restricted={false} component={LoginForm} path="/" exact />
        <PrivateRoot component={LibraryItems} path="/library-items" exact />
      </Switch>
    </main>
  );
};

export default App;
